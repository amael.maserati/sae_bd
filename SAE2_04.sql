-- TP 2_04
-- Nom: Masérati , Prenom: Amaël

-- +------------------+--
-- * Question 1 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  La liste des objets vendus par ght1ordi au mois de février 2023

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +----------+----------------------+
-- | pseudout | nomob                |
-- +----------+----------------------+
-- | etc...
-- = Reponse question 1.

Select pseudoUt, nomOb from UTILISATEUR natural join OBJET natural join VENTE
natural join STATUT where pseudoUt='ght1ordi' and MONTH(finve)=2 
and YEAR(finve)=2023 and nomst="Validée";

-- +------------------+--
-- * Question 2 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  La liste des utilisateurs qui ont enchérit sur un objet qu’ils ont eux même mis en vente

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------+
-- | pseudout  |
-- +-----------+
-- | etc...
-- = Reponse question 2.

Select distinct pseudoUt from UTILISATEUR u natural join OBJET natural join VENTE where idve IN
(Select idve from ENCHERIR e where u.idut=e.idut);

-- +------------------+--
-- * Question 3 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  La liste des utilisateurs qui ont mis en vente des objets mais uniquement des meubles

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-------------+
-- | pseudout    |
-- +-------------+
-- | etc...
-- = Reponse question 3.

Select distinct pseudoUt from UTILISATEUR natural join OBJET 
natural join VENTE where idve not in
(Select idve from OBJET natural join VENTE natural join CATEGORIE where nomcat!='Meuble');

-- +------------------+--
-- * Question 4 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  La liste des objets qui ont généré plus de 15 enchères en 2022

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------+----------------------+
-- | idob | nomob                |
-- +------+----------------------+
-- | etc...
-- = Reponse question 4.

Select idob, nomob from OBJET natural join VENTE natural join ENCHERIR e where
YEAR(e.dateheure)=2022 group by idob, nomob having count(e.idve) >= 15;

-- +------------------+--
-- * Question 5 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Ici NE CREEZ PAS la vue PRIXVENTE mais indiquer simplement la requête qui lui est associée. C'est à dire la requête permettant d'obtenir pour chaque vente validée, l'identifiant de la vente l'identiant de l'acheteur et le prix de la vente.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------+------------+----------+
-- | idve | idacheteur | montant  |
-- +------+------------+----------+
-- | etc...
-- = Reponse question 5.

Select idve, idut as idacheteur, montant from ENCHERIR e1 natural join VENTE 
natural join STATUT where nomst="Validée" and montant= 
(Select max(montant) from ENCHERIR e2 where e1.idve=e2.idve) group by idve;

-- +------------------+--
-- * Question 6 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Le chiffre d’affaire par mois de la plateforme (en utilisant la vue PRIXVENTE)

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------+-------+-----------+
-- | mois | annee | ca        |
-- +------+-------+-----------+
-- | etc...
-- = Reponse question 6.

CREATE OR REPLACE VIEW PRIXVENTE as Select idve, idut as idacheteur, montant from STATUT natural join 
VENTE natural join ENCHERIR e1 where nomst='Validée' and montant= 
(Select max(montant) from ENCHERIR e2 where e1.idve=e2.idve) group by idve;

Select MONTH(dateheure) mois, YEAR(dateheure) annee, sum(montant*0.05) CA from PRIXVENTE natural join VENTE
natural join ENCHERIR group by mois, annee order by annee, mois;

-- +------------------+--
-- * Question 7 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Les informations du ou des utilisateurs qui ont mis le plus d’objets en vente

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------+----------+------+
-- | idut | pseudout | nbob |
-- +------+----------+------+
-- | etc...
-- = Reponse question 7.

Select idut, pseudout, count(idob) nbob from UTILISATEUR natural join OBJET 
natural join VENTE group by idut 
having nbob >= ALL(Select count(idob) from UTILISATEUR natural join OBJET natural join VENTE group by idut);

-- +------------------+--
-- * Question 8 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  le camembert

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-------+-------------------+-----------+
-- | idcat | nomcat            | nb_objets |
-- +-------+-------------------+-----------+
-- | etc...
-- = Reponse question 8.

select idcat, nomcat, count(idve) nb_objets from CATEGORIE natural join 
OBJET natural join VENTE natural join STATUT where YEAR(debutVe)=2022 
AND YEAR(finVe)=2022 and nomst="Validée" group by idcat, nomcat; 


-- +------------------+--
-- * Question 9 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Le top des vendeurs

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------+-------------+----------+
-- | idut | pseudout    | total    |
-- +------+-------------+----------+
-- | etc...
-- = Reponse question 9.

Select idut, pseudout, sum(montant) total from UTILISATEUR natural join OBJET natural join VENTE natural
join PRIXVENTE natural join STATUT where nomst="Validée" and MONTH(finve)=1 AND YEAR(finve)=2023 
group by idut 
order by total desc 
limit 10;